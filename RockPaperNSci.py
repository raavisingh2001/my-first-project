import random

while True:
    choices = ["rock","paper","scissors"]

    computer = random.choice(choices)
    me = None

    while me not in choices:
        me = input("rock, paper, or scissors?: ")

    if me == computer:
        print("computer: ",computer)
        print("me: ",me)
        print("Tie!")

    elif me == "rock":
        if computer == "paper":
            print("computer: ", computer)
            print("me: ", me)
            print("You lose!")
        if computer == "scissors":
            print("computer: ", computer)
            print("me: ", me)
            print("You win!")

    elif me == "scissors":
        if computer == "rock":
            print("computer: ", computer)
            print("me: ", me)
            print("You lose!")
        if computer == "paper":
            print("computer: ", computer)
            print("me: ", me)
            print("You win!")

    elif me == "paper":
        if computer == "scissors":
            print("computer: ", computer)
            print("me: ", me)
            print("You lose!")
        if computer == "rock":
            print("computer: ", computer)
            print("me: ", me)
            print("You win!")

    One_more_round = input("Wanna play again? (yes/no): ")

    if One_more_round!= "yes":
        break


